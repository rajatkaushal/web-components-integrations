import React from 'react';
import logo from './logo.svg';
import './App.css';
import './two-way-wbc';

function App() {
  return (
      <div className="react-app">
          <div className="logo-container">
              <img className="logo" src={logo}/>
          </div>
          <div className="header">React & web-components integration demo</div>
          <div className="wbc-container">
              <two-way-binding></two-way-binding>
          </div>
      </div>
  );
}

export default App;
